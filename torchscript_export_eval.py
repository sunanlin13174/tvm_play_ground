import argparse
from loguru import logger
import numpy as np
import tvm
import tvm.relay as relay
from tvm import autotvm
import tvm.relay.testing

from tvm.relay.function import Function
from tvm.relay.expr import Call, Constant, Tuple, GlobalVar, Var, TupleGetItem
from tvm.relay.expr_functor import ExprMutator, ExprVisitor
from tvm.relay.op.contrib.tensorrt import partition_for_tensorrt
import tvm.contrib.graph_executor as runtime
import onnx
import torch


def make_parser():
    parser = argparse.ArgumentParser("trt_onnx")
    parser.add_argument("-p", "--path", type=str, default=None)
    parser.add_argument("-d", "--device", type=str, default='cuda')
    return parser


if __name__ == '__main__':
    args = make_parser().parse_args()
    target = ''
    if args.device == 'cuda':
        target = tvm.target.Target("cuda", host="llvm")
    elif args.device == 'opencl':
        target = tvm.target.Target("opencl", host="llvm")
    else:
        target = "llvm -mcpu=skylake"
    print(target)

    ts_model = torch.jit.load(args.path,
                              map_location=torch.device('cpu'))
    input_name = "images"
    img = np.zeros((1, 3, 640, 640), dtype=np.float)
    shape_dict = {input_name: img.shape}
    shape_list = [(input_name, img.shape)]
    mod, params = relay.frontend.from_pytorch(ts_model, shape_list)

    with tvm.transform.PassContext(opt_level=3):
        lib = relay.build_module.build(mod, target=target, params=params)

    path = "./lib_yolov7_"+args.device+"_win.so"
    dtype = "float32"
    lib.export_library(path)
    lib = tvm.runtime.load_module(path)
    dev = tvm.device((str(target)), 0)
    module = runtime.GraphModule(lib["default"](dev))
    data_tvm = tvm.nd.array((np.random.uniform(size=img.shape)).astype(dtype))
    module.set_input("images", data_tvm)
    module.run()
    module.run()
    num_inps = module.get_num_outputs()
    for i in range(0, num_inps):
        out = module.get_output(i)
        print(out.shape)
    # evaluate
    print("Evaluate inference time cost...")
    ftimer = module.module.time_evaluator("run", dev, number=1, repeat=600)
    prof_res = np.array(ftimer().results) * 1000  # convert to millisecond
    print(
        "Mean inference time (std dev): %.2f ms (%.2f ms)"
        % (np.mean(prof_res), np.std(prof_res))
    )
